#!/usr/bin/env node

import fs from 'fs/promises';

import {
  get_dir,
  get_dir_rec,
  async_filter,
  is_dir,
  is_valid_file,
  is_js_file,
  auto_gen,
  EOL,
  SPACE
} from './lib.js';

// Index Generator
// path           : string:   Target for index file
// gen_file_list  : function: return array of files
// gen_file       : function: wrap file_list into file

export default async function () {
  const dirs = [
    // 'app/src/api',
    'server/src/db/enums',
    {
      path: rec_path('server/src/api/components'),
      gen_file_list: std_rec_file_list
    },
    {
      path: rec_path('server/src/db/models', true),
      gen_file_list: std_rec_file_list
    },
    {
      path: 'server/src/api/paths',
      gen_file: api_file,
      gen_file_list: api_file_list
    }
  ];

  await Promise.all(dirs.map(gen_index));
}

export async function gen_index (dir) {
  const path_ = await (dir.path || dir);
  const path = Array.isArray(path_) ? path_ : [path_];
  const gen_file_list = dir.gen_file_list || std_file_list;
  const gen_file = dir.gen_file || std_file;

  await Promise.all(path.map(async p => {
    const out = gen_file(await gen_file_list(p));
    await fs.writeFile(`${p}/index.js`, out);
  }));
}

export async function rec_path (path, include_root = false) {
  const paths = await async_filter(await get_dir_rec(path), async (f) => {
    return !is_js_file(f) && await is_dir(f);
  });

  if (include_root) paths.push(path);
  return paths;
}

// Standard index file
export function std_file (out = []) {
  return auto_gen() +
    out
      .map(f => `export * from './${f}';`)
      .join(EOL()) +
    EOL();
}
export async function std_file_list (path) {
  return (await get_dir(path)).filter(is_valid_file);
}
export async function std_rec_file_list (path) {
  return async_filter(await get_dir(path), async (f) => {
    return is_valid_file(f) || await is_dir(`${path}/${f}`);
  });
}

// Index file for API paths
export function api_file (file_list) {
  return auto_gen() +
    'export default [' +
    EOL() +
    file_list.map(f => SPACE(2) + f + ',').join(EOL()) +
    EOL() +
    '];' +
    EOL();
}
export async function api_file_list (dir) {
  const files = (await get_dir_rec(dir)).filter(is_valid_file);

  return files.map(f => {
    const file_path = f.split(dir)[1];
    const path = file_path.slice(0, -3); // remove .js extension
    return `{ path: '${path}', module: require('.${file_path}') }`;
  });
}
