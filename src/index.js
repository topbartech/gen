#!/usr/bin/env node

import gen_index from './gen_index.js';
import gen_icons from './gen_icons.js';
import gen_env from './gen_env.js';
import gen_client_api from './gen_client_api.js';

const command = process.argv[2];
const args = process.argv.slice(3);

switch (command) {
  case 'index': gen_index(args); break;
  case 'env': gen_env(args); break;
  case 'icons': gen_icons(args); break;
  case 'client_api': gen_client_api(args); break;
  default: console.error(`Bad command '${command}'`);
}
