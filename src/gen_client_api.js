import fs from 'fs/promises';

import snakeCase from 'lodash.snakecase';
import { parse } from 'acorn';

import {
  get_dir_rec,
  is_valid_file,
  INDENT,
  EOL,
  SPACE,
  auto_gen
} from './lib.js';
import { gen_index } from './gen_index.js';

export default async function () {
  const PATH = 'server/src/api/paths';
  const OUT_PATH = 'app/src/api';
  const files = (await get_dir_rec(PATH)).filter(is_valid_file);

  // Generated Map<file_name, Map<method, function_string>>
  const FILE_MAP = {};
  await Promise.all(files.map(async (file_name, i) => {
    const file = await fs.readFile(file_name);
    const ast = parse(file, {
      allowImportExportEverywhere: true
    });
    const methods = parse_root(ast);

    const clean_path = file_name
      .split(PATH)
      .filter(f => f.length)[0]
      .slice(file_name[0] === '/' ? 1 : 0, -3);

    const { root, functions } = gen_functions(clean_path, methods);
    functions.i = i; // maintain original file order

    if (FILE_MAP[root]) {
      FILE_MAP[root] = [
        ...FILE_MAP[root],
        ...[functions]
      ];
    } else {
      FILE_MAP[root] = [functions];
    }
  }));

  // Generate API files using FILE_MAP
  await fs.mkdir(OUT_PATH, { recursive: true });
  await Promise.all(Object.keys(FILE_MAP).map(async (file) => {
    const functions = FILE_MAP[file].sort((a, b) => a.i - b.i);

    const out_file =
      auto_gen() +
      'import { authEndpoint, headers } from \'@/util/util\';' +
      EOL() +
      'import { baseAPI } from \'@/util/config\';' +
      EOL(2) +
      'import { query_string } from \'~/util/util\';' +
      EOL(2) +
      functions.map(chunk => ['GET', 'POST', 'PUT', 'DELETE'].reduce((out, method) => {
        if (!chunk[method]) return out;
        out.push(chunk[method]);
        return out;
      }, []).join(EOL())).join(EOL(2)) +
      EOL();

    await fs.writeFile(`${OUT_PATH}/${file}.js`, out_file);
  }));

  await gen_index(OUT_PATH);
}

// Generate functions for a given path and set of methods
// return: {
//   root: first path component,
//   functions: Map<method, function_string>
// }
function gen_functions (path, methods) {
  const params = path.match(/{(\w+)}/g);
  const clean_params = params && params.map(v => v.slice(1, -1));

  const { root, name } = path_name(path);
  const functions = methods.reduce((out, method) => {
    const is_get = method === 'GET';
    const params = clean_params ? [...clean_params] : [];

    let body, query;
    switch (method) {
      case 'POST':
      case 'PUT':
        body = name;
        params.push(body);
        break;
    }
    switch (method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        query = 'query_';
        params.push(query);
        break;
    }

    const func_name = method.toLowerCase() +
      '_' +
      (!has_id_params(root, params) && is_get ? 'all_' : '') +
      name;

    const fetch_path = path.replace(/{/g, '${');
    // eslint-disable-next-line no-template-curly-in-string
    const fetch_query = query ? '${query_string(query_)}' : '';
    const fetch_body = body ? [`body: JSON.stringify(${body}),`] : [];
    const fetch_res = is_get ? ['.then(res => res.json());'] : [];

    return {
      ...out,
      [method]: 'export async function ' +
        func_name +
        SPACE(1) +
        '(' +
        params.join(', ') +
        ')' +
        ' {' +
        EOL() +
        [
          `return fetch(\`$\{baseAPI}${fetch_path}${fetch_query}\`, {`,
          ...[
              `method: '${method}',`,
              'headers: await headers(),',
              ...fetch_body
          ].map(INDENT(2)),
          '})',
          ...[
            '.then(authEndpoint)' + (fetch_res.length ? '' : ';'),
            ...fetch_res
          ].map(INDENT(2)),
        ].map(INDENT(2)).join(EOL()) +
        EOL() +
        '}'
    };
  }, {});

  return {
    root,
    functions
  };
}

function has_id_params (root, params) {
  for (const p of params) {
    if (p.match(`${root}_(id|uuid)`)) return true;
  }

  return false;
}

// Generate path root and final name
// return: {
//   root: first path component,
//   name: proper name for the path
// }
function path_name (path) {
  const name_components = path.split('/').filter(p => p && p[0] !== '{');

  let name;
  for (let i = 0, len = name_components.length; i < len; ++i) {
    if (!name) {
      name = [name_components[i]];
      continue;
    }

    const one = name[name.length - 1];
    const two = name_components[i];
    const overlap = find_overlap(one, two.split('_')[0]);

    // Apply overlap only if there is a complete overlap
    if (overlap !== -1 && (overlap === one.length || overlap === two.length)) {
      name[name.length - 1] = one.slice(0, -overlap) + two;
    } else {
      name.push(two);
    }
  }

  return {
    root: name_components[0],
    name: snakeCase(name)
  };
}

// Find overlap between end of `one` and start of `two`.
function find_overlap (one, two) {
  if (one === two) return one.length;

  let out = -1;
  let len = 1;
  while (true) {
    const pattern = one.slice(-len);
    const found = two.indexOf(pattern);

    if (found === -1) return out;

    len += found;
    if (one.slice(-len) === two.slice(0, len)) {
      out = len++;
      if (len >= one.length) return out;
    }
  }
}

/*
 * API File Parser
 */
function parse_root (node) {
  let def_export;
  for (const child of node.body) {
    if (child.type === 'ExportDefaultDeclaration') {
      def_export = child;
      break;
    }
  }
  if (!def_export) throw new Error('[root] Expected ExportDefaultDeclaration');

  return parse_root_func(def_export);
}

function parse_root_func (node) {
  let def_return;
  try {
    for (const child of node.declaration.body.body) {
      if (child.type === 'ReturnStatement') {
        def_return = child;
        break;
      }
    }
    if (!def_return) throw new Error('[parse_root_func] Expected ReturnStatement');

    return parse_root_return(def_return);
  } catch (e) {
    console.error('[parse_root_func]', e);
    throw new Error('[parse_root_func] Badly formatted function.');
  }
}

function parse_root_return (node) {
  try {
    const out = [];
    for (const prop of node.argument.properties) {
      switch (prop.key.name) {
        case 'GET':
        case 'POST':
        case 'PUT':
        case 'DELETE':
          out.push(prop.key.name);
      }
    }

    return out;
  } catch (e) {
    console.error('[parse_root_return]', e);
    throw new Error('[parse_root_return] Badly formatted function return.');
  }
}
