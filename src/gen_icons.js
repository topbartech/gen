import fs from 'fs/promises';
import { spawn } from 'child_process';

import upperFirst from 'lodash.upperfirst';
import camelCase from 'lodash.camelcase';

import {
  auto_gen,
  EOL,
  JOIN,
  INDENT
} from './lib.js';

/* Matches
 * fa_-<icon>
 * 'fa_', '<icon>'
 * $1: prefix
 * $2: icon
 */

const reg = ".*(fa[sbrdl])(-|',[[:space:]]?')([[:alnum:]-]+).*"; // POSIX ERE
const sed_expr = `s/${reg}/\\1-\\3/p`;

export default async function () {
  const find = spawn('find', [
    'app/src',
    '-type',
    'f',
  ]);
  const sed = spawn('xargs', [
    'sed',
    '-En',
    sed_expr
  ]);
  const sort = spawn('sort');
  const uniq = spawn('uniq');

  find.stdout.pipe(sed.stdin);
  sed.stdout.pipe(sort.stdin);
  sort.stdout.pipe(uniq.stdin);

  let icons;
  for await (const data of uniq.stdout) {
    icons = data.toString().split('\n').slice(0, -1);
  };

  const icon_map = {};
  for (const icon of icons) {
    const [prefix, ...icon_] = icon.split('-');
    const name = upperFirst(camelCase(icon_.join('-')));

    if (icon_map[prefix]) {
      icon_map[prefix].push(name);
    } else {
      icon_map[prefix] = [name];
    }
  }

  let file = auto_gen(1);
  for (const prefix in icon_map) {
    const import_name = get_import(prefix);

    file += EOL() + [
      'export {',
      ...icon_map[prefix].map(icon => {
        return `fa${icon} as ${prefix}${icon},`;
      }).map(INDENT(2)),
      `} from '${import_name}';`,
      EOL()
    ].reduce(JOIN);
  }

  await fs.writeFile('app/src/styles/fontawesome_icons.js', file);
}

function get_import (prefix) {
  switch (prefix) {
    case 'fas': return '@fortawesome/pro-solid-svg-icons';
    case 'far': return '@fortawesome/pro-regular-svg-icons';
    case 'fal': return '@fortawesome/pro-light-svg-icons';
    case 'fad': return '@fortawesome/pro-duotone-svg-icons';
    default: throw new Error(`Unrecognized font prefix (${prefix})`);
  }
}
