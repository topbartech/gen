import fs from 'fs/promises';

import glob from 'glob';

export const reg_is_js = /{?\w+}?.js/i;
export const reg_is_index = /index.js/i;

export function is_valid_file (file) {
  return is_js_file(file) && !is_index_file(file);
}

export function is_js_file (file) {
  return reg_is_js.test(file);
}
export function is_index_file (file) {
  return reg_is_index.test(file);
}
export async function is_dir (file) {
  return (await fs.lstat(file)).isDirectory();
}

export async function async_filter (data, filter) {
  const filt = await Promise.all(data.map(filter));
  return data.filter((_, i) => filt[i]);
}

export function EOL (n = 1) {
  return '\n'.repeat(n);
}

export function SPACE (n = 1) {
  return ' '.repeat(n);
}

export function INDENT (n = 2) {
  return (val) => {
    return SPACE(n) + val;
  };
}

export function JOIN (out, item) {
  if (item === '\n') return out + '\n';
  return out + '\n' + item;
}

export function auto_gen (n = 2) {
  return '// auto-generated (@topbartech/gen)' + EOL(n);
}

export function bin_bash (n = 2) {
  return '#!/usr/bin/env bash' + EOL(n);
}

export async function get_dir (path) {
  return (await fs.readdir(path)).sort();
}

export async function get_dir_rec (path) {
  return new Promise((resolve, reject) => {
    return glob(`${path}/**/*`, (err, out) => {
      if (err) return reject(err);
      return resolve(out.sort());
    });
  });
}
