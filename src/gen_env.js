import fs from 'fs';

import {
  bin_bash,
  EOL,
  JOIN,
} from './lib.js';

export const usage = 'Usage: gen env <envs_path> [blank | merge <env>]';

export default async function ([envs_path, command, env_path]) {
  if (!envs_path && !command) {
    throw new Error(usage);
  }

  if (!fs.existsSync(envs_path)) {
    set_env(envs_path, '');
    return;
  }

  const envs = get_envs(envs_path);
  switch (command) {
    case 'blank':
      console.log(generate_blank(envs));
      break;
    case 'merge':
      if (!env_path) throw new Error(usage);
      console.log(generate_merge(envs, env_path));
      break;
    default:
      console.log(generate_filled(envs));
      break;
  }

  set_env(envs_path, envs.reduce(group_env(), []).reduce(JOIN) + EOL());
}

function get_envs (path) {
  return fs.readFileSync(path, 'utf8')
    .split('\n')
    .filter(e => e.length)
    .map(e => e.trim())
    .sort();
}

function set_env (path, envs) {
  fs.writeFileSync(path, envs);
}

// Space / group vars by root name
function group_env () {
  let curr_root;

  return (out, env) => {
    const root_name = env.split('_')[0];
    if (!curr_root) {
      out.push(env);
      curr_root = root_name;
      return out;
    }

    if (root_name === curr_root) {
      out.push(env);
    } else {
      out.push(EOL(), env);
      curr_root = root_name;
    }

    return out;
  };
}

// Take in stdin formatted .env.local file to merge new changes
function generate_merge (envs, env_path) {
  let env_map;
  if (fs.existsSync(env_path)) {
    env_map = get_envs(env_path)
      .map(v => v.match(/export (\w+)='(.*)';/))
      .filter(v => v && envs.includes(v[1]))
      .reduce((out, [, env, val]) => {
        out[env] = val;
        return out;
      }, {});
  }

  const out = bin_bash() + generate_filled(envs, env_map);
  fs.writeFileSync(env_path, out);
}

function generate_blank (envs) {
  return envs
    .map(e => `export ${e}=;`)
    .reduce(group_env(), [])
    .reduce(JOIN);
}

function generate_filled (envs, env_map = process.env) {
  return envs
    .map(e => `export ${e}='${env_map[e] || ''}';`)
    .reduce(group_env(), [])
    .reduce(JOIN);
}
